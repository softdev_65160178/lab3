package com.mycompany.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author natta
 */
public class ProgramOXUnitTest {
    
    public ProgramOXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test 
    public void testCheckWin_X_row1_output_true(){
        char[][] table = {{'X','X','X'},{'-','-','-'},{'-','-','-'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test 
    public void testCheckWin_X_row2_output_true(){
        char[][] table = {{'-','-','-'},{'X','X','X'},{'-','-','-'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test 
    public void testCheckWin_X_row3_output_true(){
        char[][] table = {{'-','-','-'},{'-','-','-'},{'X','X','X'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test 
    public void testCheckWin_O_row1_output_true(){
        char[][] table = {{'O','O','O'},{'-','-','-'},{'-','-','-'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test 
    public void testCheckWin_O_row2_output_true(){
        char[][] table = {{'-','-','-'},{'O','O','O'},{'-','-','-'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test 
    public void testCheckWin_O_row3_output_true(){
        char[][] table = {{'-','-','-'},{'-','-','-'},{'O','O','O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test 
    public void testCheckWin_X_col1_output_true(){
        char[][] table = {{'X','-','-'},{'X','-','-'},{'X','-','-'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test 
    public void testCheckWin_X_col2_output_true(){
        char[][] table = {{'-','X','-'},{'-','X','-'},{'-','X','-'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test 
    public void testCheckWin_X_col3_output_true(){
        char[][] table = {{'-','-','X'},{'-','-','X'},{'-','-','X'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test 
    public void testCheckWin_O_col1_output_true(){
        char[][] table = {{'O','-','-'},{'O','-','-'},{'O','-','-'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test 
    public void testCheckWin_O_col2_output_true(){
        char[][] table = {{'-','O','-'},{'-','O','-'},{'-','O','-'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test 
    public void testCheckWin_O_col3_output_true(){
        char[][] table = {{'-','-','O'},{'-','-','O'},{'-','-','O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test 
    public void testCheckWin_X_Diagonal1_output_true(){
        char[][] table = {{'X','-','-'},{'-','X','-'},{'-','-','X'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test 
    public void testCheckWin_X_Diagonal2_output_true(){
        char[][] table = {{'-','-','X'},{'-','X','-'},{'X','-','-'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test 
    public void testCheckWin_O_Diagonal1_output_true(){
        char[][] table = {{'O','-','-'},{'-','O','-'},{'-','-','O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test 
    public void testCheckWin_O_Diagonal2_output_true(){
        char[][] table = {{'-','-','O'},{'-','O','-'},{'O','-','-'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckDraw_output_false(){
        char[][] table = {{'X','X','X'},{'-','-','-'},{'-','-','-'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkDraw(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test 
    public void testCheckDraw_output_true(){
        char[][] table = {{'X','X','O'},{'O','X','X'},{'X','O','O'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkDraw(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckDraw1_output_false(){
        char[][] table = {{'O','O','O'},{'O','X','X'},{'X','O','O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkDraw(table, currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testCheckDraw1_output_true(){
        char[][] table = {{'O','X','O'},{'O','X','X'},{'X','O','O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkDraw(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckDraw2_output_false(){
        char[][] table = {{'O','X','-'},{'O','X','X'},{'X','O','O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkDraw(table, currentPlayer);
        assertEquals(false,result);
    }
}
